import React, { useState, useEffect } from 'react';
import './App.css';

import images from './images-store';
import { BrowserRouter, Switch, Route, useParams, Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


function SingleImagePage() {
  const { imageName } = useParams();

  return (<div className="single-image-container">
    <div className="back-back"></div>
    <img className="back-image" src={`/images/${imageName}`} />
    <img className="main-image" src={`/images/${imageName}`} />
  </div>)
}


function random() {
  if (window.innerWidth < 768)
    return 80;

  return Math.ceil((Math.random() * 20) + 30);
}

function ImagesGallary() {
  return (<div className="main-container">



    <div className="images-container">
      <div className="background">

        {shuffle(images).map((image, i) => <LazyLoadImage key={image} src={`/images/${image}`} />)}

      </div>

      {shuffle(images).map((image, i) => <Link key={image} to={`/${image}`}><LazyLoadImage src={`/images/${image}`} style={{ maxWidth: `${random()}vw` }} /></Link>)}
    </div>

  </div>);
}


function TopNav() {
  const cvLinkAr = 'https://docs.google.com/document/d/1KvTSRBL3hmZKiw8S9Hkj9ip97yXiE8c5KDqA-QVGFfc/edit';
  const cvLinkEn = 'https://docs.google.com/document/d/1ZAv2veegfcpR9HzWfFIbssRxqd8W5zKnVT1K41av1KA/edit?usp=sharing';


  return (<nav>

    <h1> <Link to="/">  محمد أنس العش  </Link></h1>

    <div>
      <a href={cvLinkAr} target="_blank">CV بالعربية</a>
      <Link to="/englishCV">CV English</Link>
      <Link to="/gallary">معرض الصور</Link>
    </div>

  </nav>)
}

function EnglishCVPage() {

  return (<div>
    <TopNav />
    <br />
    <br />
    <br />
    <br />
    <iframe style={{ width: '100vw', height: '1000px' }} src="https://docs.google.com/document/d/e/2PACX-1vRja9fPpV-_-tDfrVDhhociUIxwWjnY-qEiPJKQx_g55BnUcIMzwXRgKYNkcCe_sKZfmJ8uX-p9B_SM/pub?embedded=true"></iframe>
  </div>)
}


function MainPage() {

  const [imageId, useImageId] = useState(Math.ceil((Math.random() * 10) + (Math.random())));


  return (<div className="main-page-container">
    <img className="main-page-image" src={`/images/${images[imageId]}`} />
  </div>)
}
function App() {
  return (<BrowserRouter>

    <Switch>

      <Route exact path="/">
        <TopNav />
        <MainPage />
      </Route>

      <Route path="/gallary">
        <TopNav />
        <ImagesGallary />
      </Route>

      <Route path="/englishCV" >
        <EnglishCVPage />
      </Route>


      <Route path="/:imageName" >
        <SingleImagePage />
      </Route>


    </Switch>

  </BrowserRouter>)
}
export default App;
